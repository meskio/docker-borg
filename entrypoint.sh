#!/bin/sh

echo -n "restrict,command=\"borg serve" >> /root/.ssh/authorized_keys
for repo in `ls /repo`
do
	echo "allow repo $repo"
	echo -n " --restrict-to-repository /repo/$repo" >> /root/.ssh/authorized_keys
done
echo "\" $KEY" >> /root/.ssh/authorized_keys

chmod 600 /root/.ssh/authorized_keys
echo "Start ssh"
/usr/sbin/sshd -D -e
