FROM docker.io/debian

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
            borgbackup openssh-server && \
    apt-get clean

ADD sshd_config /etc/ssh/sshd_config
ADD entrypoint.sh /entrypoint.sh

RUN mkdir /repo && \
    mkdir /run/sshd && \
    mkdir /root/.ssh && \
    chmod 700 /root/.ssh

VOLUME /repo
ENTRYPOINT ["/entrypoint.sh"]
