Simple borg+ssh docker image

It will limit the ssh so only `borg serve` command will be allowed.

It will run the ssh daemon on port 2222, expect a folder with the repos in /repo and the ssh public key being passed as the environment variable `KEY`.

For example it can be run with podman:
```
podman run -p 2222:2222 \
           -e "KEY=ssh-ed25519 <key> user" \
           -v '/path/to/repo:/repo' \
           borg
```


*Warning*: This image contains the ssh private keys inside, do not redistribute it or you will leak them.
